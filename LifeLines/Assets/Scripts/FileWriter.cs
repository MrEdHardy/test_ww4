﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Corountine responsible for sending data to the php script
//author ~~JonathanK.
public class FileWriter : MonoBehaviour
{
    public IEnumerator SendToFile(string jasonstring, string timestamp) {
        
            bool successful = true;

            WWWForm form = new WWWForm();
            form.AddField("jason", jasonstring);
            form.AddField("timestamp", timestamp);

            WWW www = new WWW("http://localhost:9001/fromunity.php", form);

            yield return www;

            if (www.error != null) {
                successful = false;
            }
            else {
                Debug.Log(www.text);
                successful = true;
            }
        }
}
