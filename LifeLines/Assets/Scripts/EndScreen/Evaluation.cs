﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Evaluation : MonoBehaviour
{
    public Canvas main;
    public Canvas male, female, diverse;
    public Canvas livingroom;
    public Canvas hobby1, hobby2, hobby3, hobby4, Pic;
    private string vdHobby1, vdHobby2, vdHobby3, vdHobby4, vdlivingroom;
    private float age;

    Sprite car, social, pc, lazy, natur, animals, power, athletic, compSport, literatur, music, theater;
    //livingroom sprites
    Sprite lRS, lRXS, lRM, lRL, lRXL, adventure, chill, money;
    string actualgender = "";
    public Text textAge, textWohnort, textGraduation, textProfession, jobSelection;
    private int pic;
    private string gender;

    ValueDump vd = new ValueDump();

    // Start is called before the first frame update
    void Start()
    {
        //setactive startcanvas
        main.gameObject.SetActive(true);
        
        diverse.gameObject.SetActive(false);
        male.gameObject.SetActive(false);
        female.gameObject.SetActive(false);
        hobby1.gameObject.SetActive(true);
        hobby2.gameObject.SetActive(true);
        hobby3.gameObject.SetActive(true);
        hobby4.gameObject.SetActive(true);
        livingroom.gameObject.SetActive(true);

        //load methods
        LoadValueDump();
        StartGender();
        LoadHobby();
        LoadTextFacts();
        LoadLivingroom();
        Picloader();


    }

   
    // load all needed valuedumps and save it in vars
    void LoadValueDump() {
        
        gender = vd.GetGenderChar();        
        
        vdHobby1 = vd.GetHobby1();
        vdHobby2 = vd.GetHobby2();
        vdHobby3 = vd.GetHobby3();
        vdHobby4 = vd.GetHobby4();
        age = vd.GetAge();
        vdlivingroom = vd.GetLivingRoom();
        pic = vd.GetPic();


    }
    //get gender and activate the right canvas
    void StartGender() {
        /**if (gender == 0) {
            actualgender = "male";
        } else if (gender == 1) {
            actualgender = "female";
        } else if (gender == 2) {
            actualgender = "diverse";
        }**/
        actualgender = gender;
        switch (actualgender) {
            case "male":
                male.gameObject.SetActive(true);
                break;
            case "female":
                female.gameObject.SetActive(true);
                break;
            case "diverse":
                diverse.gameObject.SetActive(true);
                break;
        }
    }

    void LoadHobby() {

        //load all hobbie assets


        car = Resources.Load<Sprite>("Hobbies/Auto");
        social = Resources.Load<Sprite>("Hobbies/Internet_Social_Media");
        pc = Resources.Load<Sprite>("Hobbies/PC");
        lazy = Resources.Load<Sprite>("Hobbies/Faulpelz");
        natur = Resources.Load<Sprite>("Hobbies/Garten_Natur");
        animals = Resources.Load<Sprite>("Hobbies/Tiere");
        power = Resources.Load<Sprite>("Hobbies/Kraftsport");
        athletic = Resources.Load<Sprite>("Hobbies/Leichtatheltik");
        compSport = Resources.Load<Sprite>("Hobbies/Wettkampfsport");
        literatur = Resources.Load<Sprite>("Hobbies/Literatur");
        music = Resources.Load<Sprite>("Hobbies/Musik");
        theater = Resources.Load<Sprite>("Hobbies/Theater_Film");

        //give the canvas the right hobbieasset
        
        switch (vdHobby1) {
            case ("Car"):
                hobby1.GetComponent<Image>().sprite = car;
                Debug.Log("It works: car");
                break;
            case ("Social Media"):
                hobby1.GetComponent<Image>().sprite = social;
                Debug.Log("It works: social media");
                break;
            case ("PC"):
                hobby1.GetComponent<Image>().sprite = pc;
                Debug.Log("It works: pc");
                break;
            case ("lazybones"):
                hobby1.GetComponent<Image>().sprite = lazy;
                Debug.Log("It works: lazybones");
                break;
            case ("Natur"):
                hobby1.GetComponent<Image>().sprite = natur;
                Debug.Log("It works: natur");
                break;
            case ("Animals"):
                hobby1.GetComponent<Image>().sprite = animals;
                Debug.Log("It works: animals");
                break;
            case ("PowerSport"):
                hobby1.GetComponent<Image>().sprite = power;
                Debug.Log("It works: powersport");
                break;
            case ("athletics"):
                hobby1.GetComponent<Image>().sprite = athletic;
                Debug.Log("It works: athletics");
                break;
            case ("competitiveSport"):
                hobby1.GetComponent<Image>().sprite = compSport;
                Debug.Log("It works: compsport");
                break;
            case ("Literatur"):
                hobby1.GetComponent<Image>().sprite = literatur;
                Debug.Log("It works: literature");
                break;
            case ("Music"):
                hobby1.GetComponent<Image>().sprite = music;
                Debug.Log("It works: music");
                break;
            case ("Theater"):
                hobby1.GetComponent<Image>().sprite = theater;
                Debug.Log("It works: theater");
                break;
        }
        switch (vdHobby2) {
            case ("Car"):
                hobby2.GetComponent<Image>().sprite = car;
                Debug.Log("It works: car");
                break;
            case ("Social Media"):
                hobby2.GetComponent<Image>().sprite = social;
                Debug.Log("It works: social");
                break;
            case ("PC"):
                hobby2.GetComponent<Image>().sprite = pc;
                Debug.Log("It works: pc");
                break;
            case ("lazybones"):
                hobby2.GetComponent<Image>().sprite = lazy;
                Debug.Log("It works: lazy");
                break;
            case ("Natur"):
                hobby2.GetComponent<Image>().sprite = natur;
                Debug.Log("It works: natur");
                break;
            case ("Animals"):
                hobby2.GetComponent<Image>().sprite = animals;
                Debug.Log("It works: anim");
                break;
            case ("PowerSport"):
                hobby2.GetComponent<Image>().sprite = power;
                Debug.Log("It works: ps");
                break;
            case ("athletics"):
                hobby2.GetComponent<Image>().sprite = athletic;
                Debug.Log("It works: ath");
                break;
            case ("competitiveSport"):
                hobby2.GetComponent<Image>().sprite = compSport;
                Debug.Log("It works: comp");
                break;
            case ("Literatur"):
                hobby2.GetComponent<Image>().sprite = literatur;
                Debug.Log("It works: lit");
                break;
            case ("Music"):
                hobby2.GetComponent<Image>().sprite = music;
                Debug.Log("It works: music");
                break;
            case ("Theater"):
                hobby2.GetComponent<Image>().sprite = theater;
                Debug.Log("It works: theater");
                break;
        }
        switch (vdHobby3) {
            case ("Car"):
                hobby3.GetComponent<Image>().sprite = car;
                Debug.Log("It works: car");
                break;
            case ("Social Media"):
                hobby3.GetComponent<Image>().sprite = social;
                Debug.Log("It works: scm");
                break;
            case ("PC"):
                hobby3.GetComponent<Image>().sprite = pc;
                Debug.Log("It works: pc");
                break;
            case ("lazybones"):
                hobby3.GetComponent<Image>().sprite = lazy;
                Debug.Log("It works: lazy");
                break;
            case ("Natur"):
                hobby3.GetComponent<Image>().sprite = natur;
                break;
            case ("Animals"):
                hobby3.GetComponent<Image>().sprite = animals;
                Debug.Log("It works: nat");
                break;
            case ("PowerSport"):
                hobby3.GetComponent<Image>().sprite = power;
                Debug.Log("It works: ps");
                break;
            case ("athletics"):
                hobby3.GetComponent<Image>().sprite = athletic;
                Debug.Log("It works: ath");
                break;
            case ("competitiveSport"):
                hobby3.GetComponent<Image>().sprite = compSport;
                Debug.Log("It works: comp");
                break;
            case ("Literatur"):
                hobby3.GetComponent<Image>().sprite = literatur;
                Debug.Log("It works: lit");
                break;
            case ("Music"):
                hobby3.GetComponent<Image>().sprite = music;
                Debug.Log("It works: music");
                break;
            case ("Theater"):
                hobby3.GetComponent<Image>().sprite = theater;
                Debug.Log("It works: th");
                break;
        }
        switch (vdHobby4) {
            case ("Car"):
                hobby4.GetComponent<Image>().sprite = car;
                break;
            case ("Social Media"):
                hobby4.GetComponent<Image>().sprite = social;
                break;
            case ("PC"):
                hobby4.GetComponent<Image>().sprite = pc;
                break;
            case ("lazybones"):
                hobby4.GetComponent<Image>().sprite = lazy;
                break;
            case ("Natur"):
                hobby4.GetComponent<Image>().sprite = natur;
                break;
            case ("Animals"):
                hobby4.GetComponent<Image>().sprite = animals;
                break;
            case ("PowerSport"):
                hobby4.GetComponent<Image>().sprite = power;
                break;
            case ("athletics"):
                hobby4.GetComponent<Image>().sprite = athletic;
                break;
            case ("competitiveSport"):
                hobby4.GetComponent<Image>().sprite = compSport;
                break;
            case ("Literatur"):
                hobby4.GetComponent<Image>().sprite = literatur;
                break;
            case ("Music"):
                hobby4.GetComponent<Image>().sprite = music;
                break;
            case ("Theater"):
                hobby4.GetComponent<Image>().sprite = theater;
                break;
        }
    }
    void LoadTextFacts() {
        //load and set all textfacts
        textAge.text = "Alter: " + age;
        textWohnort.text = "Wohnort: " + vd.GetLocation();
        textGraduation.text = "Abschluss: " + vd.GetDegree();
        textProfession.text = "Bereich: " + vd.GetProfession();
        jobSelection.text = "Beruf: " + vd.GetCareer();

    }


    void LoadLivingroom() {
        //load livingroom assets
        lRL = Resources.Load<Sprite>("livingspace/L");
        lRS = Resources.Load<Sprite>("livingspace/S");
        lRXS = Resources.Load<Sprite>("livingspace/XS");
        lRM = Resources.Load<Sprite>("livingspace/M");
        lRXL = Resources.Load<Sprite>("livingspace/XL");

        Debug.Log(vdlivingroom);
        //load the right asset in the canvas
        switch (vdlivingroom) {

            case "LivingSpace = S":
                livingroom.GetComponent<Image>().sprite = lRS;
                break;
            case "LivingSpace = XS":
                livingroom.GetComponent<Image>().sprite = lRXS;
                break;
            case "LivingSpace = L":
                livingroom.GetComponent<Image>().sprite = lRL;
                break;
            case "LivingSpace = XL":
                livingroom.GetComponent<Image>().sprite = lRXL;
                Debug.Log("Loaded Sprite");
                break;
            case "LivingSpace = M":
                livingroom.GetComponent<Image>().sprite = lRM;
                break;

        }
    }

    void Picloader() {
        //load all pictures of lifestyle
        adventure = Resources.Load<Sprite>("evalscreen/3");
        chill = Resources.Load<Sprite>("evalscreen/5");
        money = Resources.Load<Sprite>("evalscreen/4");

        if(adventure == null)
        {
            Debug.Log("Failpic");
        }
        else if (chill == null)
        {
            Debug.Log("Failpic1");
        }
        else if (money == null)
        {
            Debug.Log("Failpic2");
        }
        else
        {

        }



        //set the right asset in the picture
        
        switch (pic) {

            case 1:
                Pic.GetComponent<Image>().sprite = adventure;
                break;
            case 2:
                Pic.GetComponent<Image>().sprite = chill;
                break;
            case 3:
                Pic.GetComponent<Image>().sprite = money;
                break;
        }
    }
}
