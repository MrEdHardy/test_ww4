﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonScripts : MonoBehaviour
{   
    //used to toggle canvas elements in scene2
    public GameObject currentCanvasElement, nextCanvasElement;
    [SerializeField] private ValueDump vd;

    //return to previous scene
    public void ToScene1()
    {
        if (vd.GetPorch())
        {
            SceneManager.LoadScene(7);
        }
        else{
            SceneManager.LoadScene(1);
        }
    }

    public void ToScene31()
    {
        int stuff = SceneManager.GetActiveScene().buildIndex - 2;
        SceneManager.LoadScene(stuff);
    }

    public void DisableElement()
    {
        currentCanvasElement.SetActive(false);
    }
    public void EnableElement()
    {
        nextCanvasElement.SetActive(true);
    }
}
