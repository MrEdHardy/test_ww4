﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneInitials : MonoBehaviour
{
    ValueDump vd;

   [SerializeField] private GameObject sportscar;
   [SerializeField] private GameObject offroader;
   [SerializeField] private GameObject familiecar;
   [SerializeField] private GameObject bike;
    // Start is called before the first frame update
    void Start()
    {
        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();

        string vehicle = vd.GetUsedVehicle(); // Vehicle the player came with

        switch (vehicle) //make the vehicle visible
        {
            case "Familenwagen_Karosse":
                familiecar.SetActive(true);
                break;
            case "Gelaendewagen_Karosse":
                offroader.SetActive(true);
                break;
            case "Sportwagen_Karosse":
                sportscar.SetActive(true);
                break;

            case "rrad_unten":
                bike.SetActive(true);
                break;
        }

        vd.SetUsedVehicle("");

    }

}
