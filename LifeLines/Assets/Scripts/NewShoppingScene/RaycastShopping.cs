﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RaycastShopping : MonoBehaviour
{   
    //used to handle the raycast
    //author: TimF, Jonathan, Marcel 
    private GameObject raycastetObject; //object the raycast hit

    Dictionary<string, string> dicc; //dic context; contains the context texts 

    [SerializeField] private int rayLenght = 2; // raylenght
    [SerializeField] private LayerMask layerMaskInteract; // special layer used to interact with the raycast

    [SerializeField] private Image uiCrosshair;
    [SerializeField] private List<Text> uiText;

   
    ValueDump vd;
    public GameObject dummySceneChanger;
    public Image interactTextBg; // Background for the interaction Text
    
    //Set up
    SceneTrigger sceneTriggerEndscreen;
    //DecisionHandler smartphone;


    public QuestLogHandler qlh;

    static GameObject lastinteracted;

    private void Start()
    {
        
      //  smartphone = GameObject.FindGameObjectWithTag("Handy").GetComponent<DecisionHandler>();

        vd = GameObject.FindGameObjectWithTag("ValueDump").GetComponent<ValueDump>();

        sceneTriggerEndscreen = dummySceneChanger.GetComponent<SceneTrigger>();


        dicc = vd.GetContextTexts();

        if(dicc == null)
        {
            Debug.Log("Fail");
        }
        else
        {
            //Debug.Log("lelelele" + dicc["door"].ToString());
        }
    }

    private void Update()
    {
        RaycastHit hit;
        Vector3 fwd = transform.TransformDirection(Vector3.forward); //lokale Var fwd (forward) ist forward der Kamera

        //Raycast in current forward position with raylenght and if something is hit
        if (Physics.Raycast(transform.position, fwd, out hit, rayLenght, layerMaskInteract.value)) {

            // is something hit with the tag 'Tuer'?
            if (hit.collider.CompareTag("Tuer")) {
                //set the hit object
                raycastetObject = hit.collider.gameObject;
                //change crosshair color
                CrosshairActive();

                interactTextBg.enabled = true;
                //enable and set context Text
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = dicc["door"];

                if (Input.GetKeyDown("e")) {
                    Debug.Log("i have interacted with Tür");
                    //set the status of the maindoor
                    vd.SetStatusMainDoor(1);
                    //raycastetObject.SetActive(false);
                }
            }
            else if (hit.collider.CompareTag("DummySceneChanger"))
            {
                //set the hit object
                raycastetObject = hit.collider.gameObject;
                //change crosshair color
                CrosshairActive();
                interactTextBg.enabled = true;
                //enable and set context Text
                uiText[1].enabled = true;
                //uiText[1].text = "Tür öffnen";
                uiText[1].text = "In Endbildschirm wechseln";

                if (Input.GetKeyDown("e"))
                {
                    raycastetObject.GetComponent<SceneTrigger>().changeScene();
                }
            }

        } else {
            //normal state of the crosshair
            CrosshairNormal();
            interactTextBg.enabled = false;
            uiText[0].text = "";

            //disable all the Context Texts //zukunft iwan mal als for oder foreach schleife!!
            uiText[0].enabled = false;
            uiText[1].enabled = false;
        }
    }

    void CrosshairActive()
    {
        uiCrosshair.color = Color.red;
    }
    void CrosshairNormal()
    {

        uiCrosshair.color = Color.white;
    }
    public void ResetDecisions()
    {   
        if(lastinteracted != null)
        {
            /**
            if (lastinteracted.GetComponent<DecisionHandler>().theme.Equals("Kommdevice"))
            {
                laptop.SetActive(true);
                briefumschlag.SetActive(true);
                handy.SetActive(true);

                laptop.GetComponent<BoxCollider>().enabled = true;
                briefumschlag.GetComponent<BoxCollider>().enabled = true;
                handy.GetComponent<BoxCollider>().enabled = true;

                qlh.ReverseDecision();

                
            }
            else if (lastinteracted.GetComponent<DecisionHandler>().theme.Equals("Food"))
            {
                sandwichObject.SetActive(true);
                schokolade.SetActive(true);
                apfel.SetActive(true);
                bananaObject.SetActive(true);

                bananaObject.GetComponent<BoxCollider>().enabled = true;
                sandwichObject.GetComponent<BoxCollider>().enabled = true;
                schokolade.GetComponent<BoxCollider>().enabled = true;
                apfel.GetComponent<BoxCollider>().enabled = true;

                qlh.ReverseDecision();

                
            }
            **/
        }
        else
        {
            Debug.Log("Fail");
        }

        lastinteracted = null;
        
    }
}
