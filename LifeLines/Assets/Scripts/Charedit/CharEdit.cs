﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CharEdit : MonoBehaviour {
    internal float value;
    //Age and Skin Men
    public Canvas menDarkOld, menDarkMid, menDarkYoung, menMidOld, menMidMid, menMidYoung, menBrightOld, menBrightMid, menBrightYoung;
    //ClothesMen
    public Canvas hairSmoothBlond, hairSmoothBrown, hairSmoothBlack, hairShoulderBlond, hairShoulderBrown, hairShoulderBlack, hairWavesBlond,
                    hairWavesBrown, hairWavesBlack, topShirtBlue, topShirtGray, topLongBlue, topLongRed, topShortBlue, topShortRed, pantsShortBlue,
                    pantsShortGray, pantsLongBlue, pantsLongGray, footShortBlue, footShortGray, footLongBlue, footLongGray;

    //Age and Skin Men
    public Canvas femDarkOld, femDarkMid, femDarkYoung, femBrightOld, femBrightMid, femBrightYoung, femMidOld, femMidMid, femMidYoung;

    //ClothesFemale
    public Canvas femHairShortBlond, femHairShortBrown, femHairShortBlack, femHairLongBlond, femHairLongBrown, femHairLongBlack, femHairMidBlond,
                    femHairMidBrown, femHairMidBlack, femTopShirtBlue, femTopShirtGray, femTopLongBlue, femTopLongRed, femTopShortBlue,
                    femTopShortRed, femPantsShortBlue, femPantsShortRed, femPantsLongBlue, femPantsLongRed, femPantsSkirtBlue, femPantsSkirtRed,
                    femFootBallGray, femFootBallRed, femFootOpen, femFootSneakGray, femFootSneakRed;

    //haircolorbuttons
    public Canvas btnBlondM, btnBrownM, btnBlackM, btnBlondW, btnBrownW, btnBlackW;
    //gendercanvas
    public Canvas symbolMale, symbolFemale;
    //dressparentcanvas and skincanvas
    public Canvas maleSkin, femaleSkin, femaleDress, maleDress;
    //sceneparents
    public Canvas backgroundChar, backgroundHobby;
    //hobbycanvas
    public Canvas car, socialMedia, pc, lazybones, natur, animals, powerSport, atheltics, comSport, literatur, music, theater;
    //float of the age slider
    private float sliderAgeValue;
    //values of skin and hair
    private int skinValue, hairValue;
    //counter for the arrows
    private int arrowCounterHairMen = 0, arrowCounterTopMen = 0, arrowCounterPantsMen = 0, arrowCounterFootMen = 0, arrowCounterHairFem = 0, arrowCounterTopFem = 0, arrowCounterPantsFem = 0, arrowCounterFootFem = 0;
    //gender
    private bool GenderMale = true;
    private bool GenderFemale = false;
    //for vd
    private string haircolor, hairstyle, charSkin, charTop, charPants, charShoes;
    string gender;
    bool hasPorch = false;

    Sprite SpriteCar, SpriteSocial, SpritePc, SpriteLazy, SpriteNatur, SpriteAnimals, SpritePower, SpriteAthletic, SpriteCompSport, SpriteLiteratur, SpriteMusic, SpriteTheater;
    Sprite SpriteCarSW, SpriteSocialSW, SpritePcSW, SpriteLazySW, SpriteNaturSW, SpriteAnimalsSW, SpritePowerSW, SpriteAthleticSW, SpriteCompSportSW, SpriteLiteraturSW, SpriteMusicSW, SpriteTheaterSW;

    private string livingSpace;

    SceneTrigger sceneTrigger = new SceneTrigger();
    ValueDump vd = new ValueDump();
    string[] StringHobby;

    // Start is called before the first frame update
    void Start() {
        StringHobby = new string[4];
        Cursor.visible = true;
        
        //activate the start canvas
        symbolMale.gameObject.SetActive(true);
        symbolFemale.gameObject.SetActive(true);
        hairSmoothBlond.gameObject.SetActive(true);
        topShirtBlue.gameObject.SetActive(true);
        pantsShortBlue.gameObject.SetActive(true);
        footShortBlue.gameObject.SetActive(true);
        btnBlondM.gameObject.SetActive(true);
        btnBrownM.gameObject.SetActive(true);
        btnBlackM.gameObject.SetActive(true);
        btnBlondW.gameObject.SetActive(false);
        btnBrownW.gameObject.SetActive(false);
        btnBlackW.gameObject.SetActive(false);
        backgroundHobby.gameObject.SetActive(false);
        backgroundChar.gameObject.SetActive(true);
    }

    // Update is called once per frame
    void Update() {
        //Debug.Log(sliderAgeValue);
        //set the right skin/age on char if the ageslider is moved or a skin button is clicked
        if (GenderMale == true) {
            femaleSkin.gameObject.SetActive(false);
            maleSkin.gameObject.SetActive(true);
            if (sliderAgeValue <= 35) {
                if (skinValue == 0) {
                menDarkYoung.gameObject.SetActive(false);
                menDarkMid.gameObject.SetActive(false);
                menDarkOld.gameObject.SetActive(false);
                menBrightYoung.gameObject.SetActive(true);
                menBrightMid.gameObject.SetActive(false);
                menBrightOld.gameObject.SetActive(false);
                menMidYoung.gameObject.SetActive(false);
                menMidMid.gameObject.SetActive(false);
                menMidOld.gameObject.SetActive(false);
                } else
                if (skinValue == 1) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(true);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);

                } else
                if (skinValue == 2) {
                    menDarkYoung.gameObject.SetActive(true);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                }

            } else if (sliderAgeValue > 35 && sliderAgeValue <= 50) {
                if (skinValue == 0) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(true);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                } else
                if (skinValue == 1) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(true);
                    menMidOld.gameObject.SetActive(false);

                } else
                if (skinValue == 2) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(true);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                }
            } else if (sliderAgeValue > 50) {
                if (skinValue == 0) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(true);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                } else
                if (skinValue == 1) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(false);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(true);

                } else
                if (skinValue == 2) {
                    menDarkYoung.gameObject.SetActive(false);
                    menDarkMid.gameObject.SetActive(false);
                    menDarkOld.gameObject.SetActive(true);
                    menBrightYoung.gameObject.SetActive(false);
                    menBrightMid.gameObject.SetActive(false);
                    menBrightOld.gameObject.SetActive(false);
                    menMidYoung.gameObject.SetActive(false);
                    menMidMid.gameObject.SetActive(false);
                    menMidOld.gameObject.SetActive(false);
                }
            }
        }
        if (GenderFemale == true) {
            maleSkin.gameObject.SetActive(false);
            femaleSkin.gameObject.SetActive(true);
            if (sliderAgeValue <= 35) {
                if (skinValue == 0) {
                    Debug.Log("is loaded");
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(true);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                } else
                if (skinValue == 1) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(true);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);

                } else
                if (skinValue == 2) {
                    femDarkYoung.gameObject.SetActive(true);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                }

            } else if (sliderAgeValue > 35 && sliderAgeValue <= 50) {
                if (skinValue == 0) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(true);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                } else
                if (skinValue == 1) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(true);
                    femMidOld.gameObject.SetActive(false);
               
                } else
                if (skinValue == 2) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(true);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                }
            } else if (sliderAgeValue > 50) {
                if (skinValue == 0) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(true);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                } else
                if (skinValue == 1) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(false);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(true);

                } else
                if (skinValue == 2) {
                    femDarkYoung.gameObject.SetActive(false);
                    femDarkMid.gameObject.SetActive(false);
                    femDarkOld.gameObject.SetActive(true);
                    femBrightYoung.gameObject.SetActive(false);
                    femBrightMid.gameObject.SetActive(false);
                    femBrightOld.gameObject.SetActive(false);
                    femMidYoung.gameObject.SetActive(false);
                    femMidMid.gameObject.SetActive(false);
                    femMidOld.gameObject.SetActive(false);
                }
            }
        }
    }

    public void OnClickSkin(String name) {
        // 0 = bright, 1 = mid, 2 = dark
        //set the skin of char
        switch (name) {

            case "Dark":
                skinValue = 2;
                charSkin = "Dark";
                break;
            case "Bright":
                skinValue = 0;
                charSkin = "Bright";
                break;
            case "Mid":
                skinValue = 1;
                charSkin = "MidHell";
                break;
        }
    }

    public void OnClickLivSpace(String name) {
        //set the livingspace
        livingSpace = "LivingSpace = " + name;
        Debug.Log(livingSpace);


    }
    

    public void OnCLickHairColor(String name) {
        //deactivate all haircanvas
        hairSmoothBlond.gameObject.SetActive(false);
        hairSmoothBrown.gameObject.SetActive(false);
        hairSmoothBlack.gameObject.SetActive(false);
        hairShoulderBlond.gameObject.SetActive(false);
        hairShoulderBrown.gameObject.SetActive(false);
        hairShoulderBlack.gameObject.SetActive(false);
        hairWavesBlond.gameObject.SetActive(false);
        hairWavesBrown.gameObject.SetActive(false);
        hairWavesBlack.gameObject.SetActive(false);

        femHairShortBlond.gameObject.SetActive(false);
        femHairShortBrown.gameObject.SetActive(false);
        femHairShortBlack.gameObject.SetActive(false);
        femHairLongBlond.gameObject.SetActive(false);
        femHairLongBrown.gameObject.SetActive(false);
        femHairLongBlack.gameObject.SetActive(false);
        femHairMidBlond.gameObject.SetActive(false);
        femHairMidBrown.gameObject.SetActive(false);
        femHairMidBlack.gameObject.SetActive(false);
        //set the haircolor
        if (GenderMale == true) {
            switch (name) {

                case "Black":
                    hairValue = 2;
                    hairSmoothBlack.gameObject.SetActive(true);
                    Debug.Log("its black");

                    break;
                case "Blond":
                    hairValue = 0;
                    Debug.Log("its blond");
                    hairSmoothBlond.gameObject.SetActive(true);
                    break;
                case "Brown":
                    hairValue = 1;
                    Debug.Log("its brown");
                    hairSmoothBrown.gameObject.SetActive(true);
                    break;
            }
        } else { 
            if (GenderFemale == true) {
                switch (name) {

                    case "Black":
                        hairValue = 2;
                        Debug.Log("its black");
                        femHairShortBlack.gameObject.SetActive(true);
                        break;
                    case "Blond":
                        hairValue = 0;
                        Debug.Log("its blond");
                        femHairShortBlond.gameObject.SetActive(true);
                        break;
                    case "Brown":
                        hairValue = 1;
                        Debug.Log("its brown");
                        femHairShortBrown.gameObject.SetActive(true);
                        break;
                }
            }
        }
        haircolor = name;
    }
    //slider for the age
    public void SliderAge(float valueAge) {
        //value from 4-12
        //value *5 for 20-60 years
        //valueAge = valueAge * 5;
        Debug.Log(valueAge);
        //safe the age in the slidervalueage
        sliderAgeValue = valueAge;

    }
    //function for the clotharrows
    public void OnClickArrow(String name) {
        
        Debug.Log(name);
        if (GenderMale == true) { 
        
            hairSmoothBlond.gameObject.SetActive(false);
            hairSmoothBrown.gameObject.SetActive(false);
            hairSmoothBlack.gameObject.SetActive(false);
            hairShoulderBlond.gameObject.SetActive(false);
            hairShoulderBrown.gameObject.SetActive(false);
            hairShoulderBlack.gameObject.SetActive(false);
            hairWavesBlond.gameObject.SetActive(false);
            hairWavesBrown.gameObject.SetActive(false);
            hairWavesBlack.gameObject.SetActive(false);

            topShirtBlue.gameObject.SetActive(false);
            topShirtGray.gameObject.SetActive(false);
            topLongBlue.gameObject.SetActive(false);
            topLongRed.gameObject.SetActive(false);
            topShortBlue.gameObject.SetActive(false);
            topShortRed.gameObject.SetActive(false);

            pantsShortBlue.gameObject.SetActive(false);
            pantsShortGray.gameObject.SetActive(false);
            pantsLongBlue.gameObject.SetActive(false);
            pantsLongGray.gameObject.SetActive(false);

            footShortBlue.gameObject.SetActive(false);
            footShortGray.gameObject.SetActive(false);
            footLongBlue.gameObject.SetActive(false);
            footLongGray.gameObject.SetActive(false);

            if (name == "arrowlefthair") {

                arrowCounterHairMen--;

                if (arrowCounterHairMen == -1) {

                    arrowCounterHairMen = 2;

                }
            }
            if (name == "arrowrighthair") {

                arrowCounterHairMen++;
                if (arrowCounterHairMen == 3) {
                    arrowCounterHairMen = 0;
                }

            }
            //hairValue = color
            if (hairValue == 0) {

                switch (arrowCounterHairMen) {

                    case 0:
                        hairSmoothBlond.gameObject.SetActive(true);                        
                        break;
                    
                    case 1:
                        hairShoulderBlond.gameObject.SetActive(true);                        
                        break;
                    
                    case 2:
                        hairWavesBlond.gameObject.SetActive(true);                        
                        break;
                }

            }
            if (hairValue == 1) {

                switch (arrowCounterHairMen) {

                    case 0:
                        hairSmoothBrown.gameObject.SetActive(true);
                        
                        break;

                    case 1:
                        hairShoulderBrown.gameObject.SetActive(true);
                        break;

                    case 2:
                        hairWavesBrown.gameObject.SetActive(true);
                        break;
                }

            }
            if (hairValue == 2) {

                switch (arrowCounterHairMen) {

                    case 0:
                        hairSmoothBlack.gameObject.SetActive(true);
                        break;

                    case 1:
                        hairShoulderBlack.gameObject.SetActive(true);
                        break;

                    case 2:
                        hairWavesBlack.gameObject.SetActive(true);
                        break;
                }

            }
            switch (arrowCounterHairMen) {

                case 0:
                    hairstyle = "Smooth";
                    break;

                case 1:
                    hairstyle = "Shoulder";
                    break;

                case 2:
                    hairstyle = "Waves";
                    break;
            }

            if (name == "arrowlefttop") {

                arrowCounterTopMen--;

                if (arrowCounterTopMen == -1) {

                    arrowCounterTopMen = 5;

                }
            }
            if (name == "arrowrighttop") {

                arrowCounterTopMen++;
                if (arrowCounterTopMen == 6) {
                    arrowCounterTopMen = 0;
                }

            }

            switch (arrowCounterTopMen) {

                case 0:
                    topShirtBlue.gameObject.SetActive(true);
                    charTop = "ShirtBlue";
                    break;
                case 1:
                    topShirtGray.gameObject.SetActive(true);
                    charTop = "ShirtGray";
                    break;
                case 2:
                    topLongBlue.gameObject.SetActive(true);
                    charTop = "longBlue";
                    break;
                case 3:
                    topLongRed.gameObject.SetActive(true);
                    charTop = "longRed";
                    break;
                case 4:
                    topShortBlue.gameObject.SetActive(true);
                    charTop = "ShortBlue";
                    break;
                case 5:
                    topShortRed.gameObject.SetActive(true);
                    charTop = "ShortRed";
                    break;
            }

            if (name == "arrowleftpants") {

                arrowCounterPantsMen--;

                if (arrowCounterPantsMen == -1) {

                    arrowCounterPantsMen = 3;

                }
            }
            if (name == "arrowrightpants") {

                arrowCounterPantsMen++;
                if (arrowCounterPantsMen == 4) {
                    arrowCounterPantsMen = 0;
                }

            }

            switch (arrowCounterPantsMen) {

                case 0:
                    pantsShortBlue.gameObject.SetActive(true);
                    charPants = "ShortBlue";
                    break;
                case 1:
                    pantsShortGray.gameObject.SetActive(true);
                    charPants = "ShortGray";
                    break;
                case 2:
                    pantsLongBlue.gameObject.SetActive(true);
                    charPants = "LongBlue";
                    break;
                case 3:
                    pantsLongGray.gameObject.SetActive(true);
                    charPants = "LongGray";
                    break;
            }

            if (name == "arrowleftshoes") {

                arrowCounterFootMen--;

                if (arrowCounterFootMen == -1) {

                    arrowCounterFootMen = 1;

                }
            }
            if (name == "arrowrightshoes") {

                arrowCounterFootMen++;
                if (arrowCounterFootMen == 2) {
                    arrowCounterFootMen = 0;
                }

            }


            if (arrowCounterPantsMen == 0 || arrowCounterPantsMen == 1) {

                switch (arrowCounterFootMen) {

                    case 0:
                        footShortBlue.gameObject.SetActive(true);
                        charShoes = "ShortBlue";
                        break;
                    case 1:
                        footShortGray.gameObject.SetActive(true);
                        charShoes = "ShortGray";
                        break;
                }

            }
            if (arrowCounterPantsMen == 2 || arrowCounterPantsMen == 3) {

                switch (arrowCounterFootMen) {

                    case 0:
                        footLongBlue.gameObject.SetActive(true);
                        charShoes = "LongBlue";
                        break;
                    case 1:
                        footLongGray.gameObject.SetActive(true);
                        charShoes = "LongGray";
                        break;
                }
            }
        }
        if (GenderFemale == true) {

            femHairShortBlond.gameObject.SetActive(false);
            femHairShortBrown.gameObject.SetActive(false);
            femHairShortBlack.gameObject.SetActive(false);
            femHairLongBlond.gameObject.SetActive(false);
            femHairLongBrown.gameObject.SetActive(false);
            femHairLongBlack.gameObject.SetActive(false);
            femHairMidBlond.gameObject.SetActive(false);
            femHairMidBrown.gameObject.SetActive(false);
            femHairMidBlack.gameObject.SetActive(false);

            femTopShirtBlue.gameObject.SetActive(false);
            femTopShirtGray.gameObject.SetActive(false);
            femTopLongBlue.gameObject.SetActive(false);
            femTopLongRed.gameObject.SetActive(false);
            femTopShortBlue.gameObject.SetActive(false);
            femTopShortRed.gameObject.SetActive(false);

            femPantsShortBlue.gameObject.SetActive(false);
            femPantsShortRed.gameObject.SetActive(false);
            femPantsLongBlue.gameObject.SetActive(false);
            femPantsLongRed.gameObject.SetActive(false);
            femPantsSkirtBlue.gameObject.SetActive(false);
            femPantsSkirtRed.gameObject.SetActive(false);

            femFootBallGray.gameObject.SetActive(false);
            femFootBallRed.gameObject.SetActive(false);
            femFootOpen.gameObject.SetActive(false);
            femFootSneakGray.gameObject.SetActive(false);
            femFootSneakRed.gameObject.SetActive(false);


            if (name == "arrowlefthair") {

                arrowCounterHairFem--;

                if (arrowCounterHairFem == -1) {

                    arrowCounterHairFem = 2;

                }
            }
            if (name == "arrowrighthair") {

                arrowCounterHairFem++;
                if (arrowCounterHairFem == 3) {
                    arrowCounterHairFem = 0;
                }

            }
            
            if (hairValue == 0) {

                switch (arrowCounterHairFem) {

                    case 0:
                        femHairShortBlond.gameObject.SetActive(true);
                        break;

                    case 1:
                        femHairLongBlond.gameObject.SetActive(true);
                        break;

                    case 2:
                        femHairMidBlond.gameObject.SetActive(true);
                        break;
                }

            }
            if (hairValue == 1) {

                switch (arrowCounterHairFem) {

                    case 0:
                        femHairShortBrown.gameObject.SetActive(true);
                        break;

                    case 1:
                        femHairLongBrown.gameObject.SetActive(true);
                        break;

                    case 2:
                        femHairMidBrown.gameObject.SetActive(true);
                        break;
                }

            }
            if (hairValue == 2) {

                switch (arrowCounterHairFem) {

                    case 0:
                        femHairShortBlack.gameObject.SetActive(true);
                        break;

                    case 1:
                        femHairLongBlack.gameObject.SetActive(true);
                        break;

                    case 2:
                        femHairMidBlack.gameObject.SetActive(true);
                        break;
                }

            }
            switch (arrowCounterHairFem) {

                case 0:
                    hairstyle = "Short";
                    break;

                case 1:
                    hairstyle = "Long";
                    break;

                case 2:
                    hairstyle = "MidLong";
                    break;
            }

            if (name == "arrowlefttop") {

                arrowCounterTopFem--;

                if (arrowCounterTopFem == -1) {

                    arrowCounterTopFem = 5;

                }
            }
            if (name == "arrowrighttop") {

                arrowCounterTopFem++;
                if (arrowCounterTopFem == 6) {
                    arrowCounterTopFem = 0;
                }

            }

            switch (arrowCounterTopFem) {

                case 0:
                    femTopShirtBlue.gameObject.SetActive(true);
                    charTop = "ShirtBlue";
                    break;
                case 1:
                    femTopShirtGray.gameObject.SetActive(true);
                    charTop = "ShirtRed";
                    break;
                case 2:
                    femTopLongBlue.gameObject.SetActive(true);
                    charTop = "LongBlue";
                    break;
                case 3:
                    femTopLongRed.gameObject.SetActive(true);
                    charTop = "LongRed";
                    break;
                case 4:
                    femTopShortBlue.gameObject.SetActive(true);
                    charTop = "ShortBlue";
                    break;
                case 5:
                    femTopShortRed.gameObject.SetActive(true);
                    charTop = "ShortRed";
                    break;
            }
            

            if (name == "arrowleftpants") {

                arrowCounterPantsFem--;

                if (arrowCounterPantsFem == -1) {

                    arrowCounterPantsFem = 5;

                }
            }
            if (name == "arrowrightpants") {

                arrowCounterPantsFem++;
                if (arrowCounterPantsFem == 6) {
                    arrowCounterPantsFem = 0;
                }

            }
            switch (arrowCounterPantsFem) {

                case 0:
                    femPantsShortBlue.gameObject.SetActive(true);
                    charPants = "ShortBlue";
                    break;
                case 1:
                    femPantsShortRed.gameObject.SetActive(true);
                    charPants = "ShortRed";
                    break;
                case 2:
                    femPantsLongBlue.gameObject.SetActive(true);
                    charPants = "LongBlue";
                    break;
                case 3:
                    femPantsLongRed.gameObject.SetActive(true);
                    charPants = "LongRed";
                    break;
                case 4:
                    femPantsSkirtBlue.gameObject.SetActive(true);
                    charPants = "SkirtBlue";
                    break;
                case 5:
                    femPantsSkirtRed.gameObject.SetActive(true);
                    charPants = "SkirtRed";
                    break;
            }
            
            if (name == "arrowleftshoes") {

                arrowCounterFootFem--;

                if (arrowCounterFootFem == -1) {

                    arrowCounterFootFem = 4;

                }
            }
            if (name == "arrowrightshoes") {

                arrowCounterFootFem++;
                if (arrowCounterFootFem == 5) {
                    arrowCounterFootFem = 0;
                }

            }
            
            switch (arrowCounterFootFem) {

                case 0:
                    femFootBallGray.gameObject.SetActive(true);
                    charShoes = "BallerinaGray";
                    break;
                case 1:
                    femFootBallRed.gameObject.SetActive(true);
                    charShoes = "BallerinaRed";
                    break;
                case 2:
                    femFootOpen.gameObject.SetActive(true);
                    charShoes = "OpenShoes";
                    break;
                case 3:
                    femFootSneakGray.gameObject.SetActive(true);
                    charShoes = "SneakerGray";
                    break;
                case 4:
                    femFootSneakRed.gameObject.SetActive(true);
                    charShoes = "BallerinaRed";
                    break;
            }

        }
        Debug.Log("TopMen" + arrowCounterTopMen);
        Debug.Log("HairMen" + arrowCounterHairMen);
        Debug.Log("PantsMen" + arrowCounterPantsMen);
        Debug.Log("ShoesMen" + arrowCounterFootMen);
    }
    //set the gender and change the canvas of skin, age, clothes and hairs
    public void OnClickGender(String name)
    {
        if (name == "female" || name == "male") {
        hairValue = 0;
        hairSmoothBlond.gameObject.SetActive(false);
        hairSmoothBrown.gameObject.SetActive(false);
        hairSmoothBlack.gameObject.SetActive(false);
        hairShoulderBlond.gameObject.SetActive(false);
        hairShoulderBrown.gameObject.SetActive(false);
        hairShoulderBlack.gameObject.SetActive(false);
        hairWavesBlond.gameObject.SetActive(false);
        hairWavesBrown.gameObject.SetActive(false);
        hairWavesBlack.gameObject.SetActive(false);

        topShirtBlue.gameObject.SetActive(false);
        topShirtGray.gameObject.SetActive(false);
        topLongBlue.gameObject.SetActive(false);
        topLongRed.gameObject.SetActive(false);
        topShortBlue.gameObject.SetActive(false);
        topShortRed.gameObject.SetActive(false);

        pantsShortBlue.gameObject.SetActive(false);
        pantsShortGray.gameObject.SetActive(false);
        pantsLongBlue.gameObject.SetActive(false);
        pantsLongGray.gameObject.SetActive(false);

        footShortBlue.gameObject.SetActive(false);
        footShortGray.gameObject.SetActive(false);
        footLongBlue.gameObject.SetActive(false);
        footLongGray.gameObject.SetActive(false);

        femHairShortBlond.gameObject.SetActive(false);
        femHairShortBrown.gameObject.SetActive(false);
        femHairShortBlack.gameObject.SetActive(false);
        femHairLongBlond.gameObject.SetActive(false);
        femHairLongBrown.gameObject.SetActive(false);
        femHairLongBlack.gameObject.SetActive(false);
        femHairMidBlond.gameObject.SetActive(false);
        femHairMidBrown.gameObject.SetActive(false);
        femHairMidBlack.gameObject.SetActive(false);

        femTopShirtBlue.gameObject.SetActive(false);
        femTopShirtGray.gameObject.SetActive(false);
        femTopLongBlue.gameObject.SetActive(false);
        femTopLongRed.gameObject.SetActive(false);
        femTopShortBlue.gameObject.SetActive(false);
        femTopShortRed.gameObject.SetActive(false);

        femPantsShortBlue.gameObject.SetActive(false);
        femPantsShortRed.gameObject.SetActive(false);
        femPantsLongBlue.gameObject.SetActive(false);
        femPantsLongRed.gameObject.SetActive(false);
        femPantsSkirtBlue.gameObject.SetActive(false);
        femPantsSkirtRed.gameObject.SetActive(false);

        femFootBallGray.gameObject.SetActive(false);
        femFootBallRed.gameObject.SetActive(false);
        femFootOpen.gameObject.SetActive(false);
        femFootSneakGray.gameObject.SetActive(false);
        femFootSneakRed.gameObject.SetActive(false);

    }
    if (name == "female") {
        GenderFemale = true;
        GenderMale = false;
        gender = "female";
        femaleDress.gameObject.SetActive(true);
        maleDress.gameObject.SetActive(false);

        femHairShortBlond.gameObject.SetActive(true);
        femTopShirtBlue.gameObject.SetActive(true);
        femPantsShortBlue.gameObject.SetActive(true);
        femFootBallGray.gameObject.SetActive(true);

        arrowCounterFootFem = 0;
        arrowCounterHairFem = 0;
        arrowCounterPantsFem = 0;
        arrowCounterTopFem = 0;
        btnBlondM.gameObject.SetActive(false);
        btnBrownM.gameObject.SetActive(false);
        btnBlackM.gameObject.SetActive(false);
        btnBlondW.gameObject.SetActive(true);
        btnBrownW.gameObject.SetActive(true);
        btnBlackW.gameObject.SetActive(true);

    }
    if (name == "male") {
        gender = "male";
        GenderFemale = false;
        GenderMale = true;
        femaleDress.gameObject.SetActive(false);
        maleDress.gameObject.SetActive(true);

        hairSmoothBlond.gameObject.SetActive(true);
        topShirtBlue.gameObject.SetActive(true);
        pantsShortBlue.gameObject.SetActive(true);
        footShortBlue.gameObject.SetActive(true);


        arrowCounterTopMen = 0;
        arrowCounterHairMen = 0;
        arrowCounterPantsMen = 0;
        arrowCounterFootMen = 0;
        btnBlondM.gameObject.SetActive(true);
        btnBrownM.gameObject.SetActive(true);
        btnBlackM.gameObject.SetActive(true);
        btnBlondW.gameObject.SetActive(false);
        btnBrownW.gameObject.SetActive(false);
        btnBlackW.gameObject.SetActive(false);
    }

     
    }

    public void TogglePorch( bool porch) {

        this.hasPorch = porch;
    }

    public void OnClickNext() {

        backgroundChar.gameObject.SetActive(false);
        backgroundHobby.gameObject.SetActive(true);
    }

    public void OnClickHobby(String name) {
        int hobbyCounter = 0;

        SpriteCar = Resources.Load<Sprite>("Hobbies/Auto");
        SpriteSocial = Resources.Load<Sprite>("Hobbies/Internet_Social_Media");
        SpritePc = Resources.Load<Sprite>("Hobbies/PC");
        SpriteLazy = Resources.Load<Sprite>("Hobbies/Faulpelz");
        SpriteNatur = Resources.Load<Sprite>("Hobbies/Garten_Natur");
        SpriteAnimals = Resources.Load<Sprite>("Hobbies/Tiere");
        SpritePower = Resources.Load<Sprite>("Hobbies/Kraftsport");
        SpriteAthletic = Resources.Load<Sprite>("Hobbies/Leichtatheltik");
        SpriteCompSport = Resources.Load<Sprite>("Hobbies/Wettkampfsport");
        SpriteLiteratur = Resources.Load<Sprite>("Hobbies/Literatur");
        SpriteMusic = Resources.Load<Sprite>("Hobbies/Musik");
        SpriteTheater = Resources.Load<Sprite>("Hobbies/Theater_Film");

        SpriteCarSW = Resources.Load<Sprite>("deAcHobbies/AutoSW");
        SpriteSocialSW = Resources.Load<Sprite>("deAcHobbies/SocialMediaSW");
        SpritePcSW = Resources.Load<Sprite>("deAcHobbies/ComputerSW");
        SpriteLazySW = Resources.Load<Sprite>("deAcHobbies/StubenhockerSW");
        SpriteNaturSW = Resources.Load<Sprite>("deAcHobbies/GartenSW");
        SpriteAnimalsSW = Resources.Load<Sprite>("deAcHobbies/TiereSW");
        SpritePowerSW = Resources.Load<Sprite>("deAcHobbies/KraftsportSW");
        SpriteAthleticSW = Resources.Load<Sprite>("deAcHobbies/LeichtatheltikSW");
        SpriteCompSportSW = Resources.Load<Sprite>("deAcHobbies/WettkampfSW");
        SpriteLiteraturSW = Resources.Load<Sprite>("deAcHobbies/LiteraturSW");
        SpriteMusicSW = Resources.Load<Sprite>("deAcHobbies/MusikSW");
        SpriteTheaterSW = Resources.Load<Sprite>("deAcHobbies/TheaterSW");
        if (hobbyCounter > 4) {

            Debug.Log("zu viele hobbys!");
        }else
        if (hobbyCounter <= 4) {
                //checked if gives a Object in the Array with this name
                if (Array.Exists(StringHobby, element => element == name)) {
                //if yes, checked here the Index
                int index = Array.IndexOf(StringHobby, name);
                StringHobby[index] = null;
                Debug.Log("index: " + index);

            
                //public Canvas car, socialMedia, pc, lazybones, natur, animals, powerSport, atheltics, comSport, literatur, music, theater;
                switch (name) {

                    case "Car":
                        car.GetComponent<Image>().sprite = SpriteCar;
                        break;
                    case ("Social Media"):
                        socialMedia.GetComponent<Image>().sprite = SpriteSocial;
                        break;
                    case ("PC"):
                        pc.GetComponent<Image>().sprite = SpritePc;
                        break;
                    case ("lazybones"):
                        lazybones.GetComponent<Image>().sprite = SpriteLazy;
                        break;
                    case ("Natur"):
                        natur.GetComponent<Image>().sprite = SpriteNatur;
                        break;
                    case ("Animals"):
                        animals.GetComponent<Image>().sprite = SpriteAnimals;
                        break;
                    case ("PowerSport"):
                        powerSport.GetComponent<Image>().sprite = SpritePower;
                        break;
                    case ("athletics"):
                        atheltics.GetComponent<Image>().sprite = SpriteAthletic;
                        break;
                    case ("competitiveSport"):
                        comSport.GetComponent<Image>().sprite = SpriteCompSport;
                        break;
                    case ("Literatur"):
                        literatur.GetComponent<Image>().sprite = SpriteLiteratur;
                        break;
                    case ("Music"):
                        music.GetComponent<Image>().sprite = SpriteMusic;
                        break;
                    case ("Theater"):
                        theater.GetComponent<Image>().sprite = SpriteTheater;
                        break;
                }

            } else {
                //if not it searched for a empty index
                int indexnull = Array.IndexOf(StringHobby, null);
                if (StringHobby[indexnull] == null) {
                    Debug.Log("indexnull: " + indexnull);
                    //write the name in the empty index
                    StringHobby[indexnull] = name;
                    hobbyCounter++;
                    switch (name) {

                        case "Car":
                            car.GetComponent<Image>().sprite = SpriteCarSW;
                            break;
                        case ("Social Media"):
                            socialMedia.GetComponent<Image>().sprite = SpriteSocialSW;
                            break;
                        case ("PC"):
                            pc.GetComponent<Image>().sprite = SpritePcSW;
                            break;
                        case ("lazybones"):
                            lazybones.GetComponent<Image>().sprite = SpriteLazySW;
                            break;
                        case ("Natur"):
                            natur.GetComponent<Image>().sprite = SpriteNaturSW;
                            break;
                        case ("Animals"):
                            animals.GetComponent<Image>().sprite = SpriteAnimalsSW;
                            break;
                        case ("PowerSport"):
                            powerSport.GetComponent<Image>().sprite = SpritePowerSW;
                            break;
                        case ("athletics"):
                            atheltics.GetComponent<Image>().sprite = SpriteAthleticSW;
                            break;
                        case ("competitiveSport"):
                            comSport.GetComponent<Image>().sprite = SpriteCompSportSW;
                            break;
                        case ("Literatur"):
                            literatur.GetComponent<Image>().sprite = SpriteLiteraturSW;
                            break;
                        case ("Music"):
                            music.GetComponent<Image>().sprite = SpriteMusicSW;
                            break;
                        case ("Theater"):
                            theater.GetComponent<Image>().sprite = SpriteTheaterSW;
                            break;
                    }

                }
            }
        Debug.Log("index0" + StringHobby[0] + "index1" + StringHobby[1] + "index2" + StringHobby[2] + "index3" + StringHobby[3]);
        
        
        }
    }


    //end of the charedit scene
    //set the valuedumps
    //load the main scene
    public void OnClickEnd() {

        if (hasPorch) //Scene Loading depending on house haveing a porche
        {
            sceneTrigger.scene = 7;
        }
        else {
            sceneTrigger.scene = 1;
        }
        
        sceneTrigger.changeScene();
        Debug.Log("Gender: " + gender);
        vd.SetGenderChar(gender);
        vd.SetHairColor(haircolor);
        vd.SetHairStyle(hairstyle);
        vd.SetCharSkin(charSkin);
        vd.SetCharTop(charTop);
        vd.SetCharPants(charPants);
        vd.SetCharShoes(charShoes);
        vd.SetLivingRoom(livingSpace);

        vd.SetHobby1(StringHobby[0]);
        vd.SetHobby2(StringHobby[1]);
        vd.SetHobby3(StringHobby[2]);
        vd.SetHobby4(StringHobby[3]);
        vd.SetAge(sliderAgeValue);

        vd.SetPorch(hasPorch);
    }

}

    

